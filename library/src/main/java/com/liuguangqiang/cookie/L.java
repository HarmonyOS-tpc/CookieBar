/*******************************************************************************
 * Copyright 2011-2014 Sergey Tarasevich
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.liuguangqiang.cookie;

import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

import java.util.logging.Logger;


public final class L {

    private static final String LOG_FORMAT = "%1$s\n%2$s";
    private static volatile boolean writeDebugLogs = false;
    private static volatile boolean writeLogs = true;

    private L() {
    }


    @Deprecated
    public static void enableLogging() {
        writeLogs(true);
    }

    @Deprecated
    public static void disableLogging() {
        writeLogs(false);
    }


    public static void writeDebugLogs(boolean writeDebugLogs) {
        L.writeDebugLogs = writeDebugLogs;
    }


    public static void writeLogs(boolean writeLogs) {
        L.writeLogs = writeLogs;
    }

    public static void d(String message, Object... args) {
        if (writeDebugLogs) {
            log(HiLog.DEBUG, null, message, args);
        }
    }

    public static void i(String message, Object... args) {
        log(HiLog.INFO, null, message, args);
    }

    public static void w(String message, Object... args) {
        log(HiLog.WARN, null, message, args);
    }

    public static void e(Throwable ex) {
        log(HiLog.ERROR, ex, null);
    }

    public static void e(String message, Object... args) {
        log(HiLog.ERROR, null, message, args);
    }

    public static void e(Throwable ex, String message, Object... args) {
        log(HiLog.ERROR, ex, message, args);
    }

    private static void log(int priority, Throwable ex, String message, Object... args) {
        if (!writeLogs) return;
        if (args.length > 0) {
            message = String.format(message, args);
        }
        String log;
        if (ex == null) {
            log = message;
        } else {
            String logMessage = message == null ? ex.getMessage() : message;
            String logBody = HiLog.getStackTrace(ex);
            log = String.format(LOG_FORMAT, logMessage, logBody);
        }
        HiLogLabel label = new HiLogLabel(priority, 0x00201, "MY_TAG");
        HiLog.info(label, "MY_TAG", log);
    }

    public static void info(String msg) {
        Logger.getGlobal().info("jwen====" + msg);
    }

}